import { createAction } from 'redux-actions';

const addProduct = createAction('ADD_PRODUCT', product => product);

export default addProduct;
