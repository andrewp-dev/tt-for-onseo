import { createAction } from 'redux-actions';

const removeProduct = createAction('REMOVE_PRODUCT', (id, name ) => ({ id, name }) );

export default removeProduct;
