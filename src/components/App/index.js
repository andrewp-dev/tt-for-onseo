import React from 'react';
import { Route } from 'react-router-dom';
import Catalog from '../Catalog';
import Cart from '../Cart';
import Header from '../Header';

function App() {
  return (
    <div className="App">
      <Header />

      <main>
        <Route exact path="/" component={ Catalog } />
        <Route exact path="/cart" component={ Cart } />
      </main>
    </div>
  );
}

export default App;
