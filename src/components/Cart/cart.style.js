import styled from 'styled-components';

const grid = {
  width: '1140px',
};

const CartWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  min-height: ${ grid.height };
  max-width: ${ grid.width };
  width: auto;
  margin: 0 auto;
`;

export default CartWrapper;