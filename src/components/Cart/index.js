import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import ListItem from '../ListItem';
import CartWrapper from './cart.style';
import { bindActionCreators } from 'redux';
import { removeProduct } from '../../actions';

class Cart extends PureComponent {
  productsList = () => {
    return this.props.products.map(product => (
      <ListItem
        key={ product.id }
        cart
        { ...product }
      />
    ));
  }

  render() {
    return (
      <CartWrapper>
        { this.productsList() }
      </CartWrapper>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products
});

const matchDispatchToProps = dispatch => bindActionCreators({
  removeProduct
}, dispatch);


export default connect(mapStateToProps, matchDispatchToProps)(Cart);
