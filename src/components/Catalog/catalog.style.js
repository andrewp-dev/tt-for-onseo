import styled from 'styled-components';

const grid = {
  width: '1140px',
};

const CatalogWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  min-height: ${ grid.height };
  max-width: ${ grid.width };
  width: auto;
  margin: 0 auto;
`;

export default CatalogWrapper;