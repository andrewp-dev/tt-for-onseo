import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import ListItem from '../ListItem';
import CatalogWrapper from './catalog.style';
import { bindActionCreators } from 'redux';
import { addProduct } from '../../actions';

class Catalog extends PureComponent {
  productsList = () => {
    return this.props.productsList.map(product => (
      <ListItem
        addProduct={ this.props.addProduct }
        key={ product.id }
        { ...product }
      />
    ));
  }

  render() {
    return (
      <CatalogWrapper>
        { this.productsList() }
      </CatalogWrapper>
    );
  }
}

const mapStateToProps = state => ({
  productsList: state.productsList
});

const matchDispatchToProps = dispatch => bindActionCreators({
  addProduct
}, dispatch);


export default connect(mapStateToProps, matchDispatchToProps)(Catalog);
