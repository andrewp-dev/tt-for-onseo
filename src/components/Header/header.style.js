import styled from 'styled-components';

const HeaderWrapper = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 75px;
  border-bottom: 1px solid #000;
`;

export default HeaderWrapper;