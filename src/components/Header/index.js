import React from 'react';
import { Link } from 'react-router-dom';
import HeaderWrapper from './header.style';

const Header = () => {
  return (
    <HeaderWrapper>
      <Link to="/">Home</Link>
      <Link to="/cart">Cart</Link>
    </HeaderWrapper>
  );
}

export default Header;
