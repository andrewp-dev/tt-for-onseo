import styled, { css } from 'styled-components';

const DefaultItem = `
    display: flex;
    align-items: center;
    justify-content: space-between;
    border: 1px solid #000;
    padding: 15px;
    box-sizing: border-box;
    margin: 15px;
`;

const CatalogItem = `
    ${ DefaultItem }

    flex-direction: column;
    width: 230px;
    height: 350px;
`;

export const CartItem = `
    ${ DefaultItem }
`;

export const Poster = styled.img`
    width: 200px;
    // margin: 0 auto;
`;

export const Button = styled.div`
    width: 200px;
    height: 75px;
    border: 1px solid #000;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    user-select: none;
    transition: all .15s ease-in-out;

    &:hover {
        background-color: rgba(0, 0, 0, 0.4);
        color: #fff;
    }

    ${ props => props.cart && css`
        display: none;
    `}
`;

const ListItemWrapper =  styled.div`
    ${ CartItem }

    ${ props => !props.cart && css`
        ${ CatalogItem }
    `}
`;

export default ListItemWrapper;