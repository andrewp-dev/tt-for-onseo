import React from 'react';
import ListItemWrapper, { Poster, Button } from './ListItem.style';

const displayName = 'ListItem';

const ListItem = ({ addProduct, photo, name, price, id, ...options }) => {
  return (
    <ListItemWrapper
      { ...options }
    >
      <Poster 
        alt='logo'
        src={ photo }
      />

      <div>{ name }</div>
      <div>{ price }$</div>

      <Button 
        onClick={ () => addProduct({ id, name, photo, price }) }
        { ...options }
      >Buy</Button>
    </ListItemWrapper>
  );
}

ListItem.displayName = displayName;

export default ListItem;
