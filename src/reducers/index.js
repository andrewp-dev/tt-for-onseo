import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import products from './products';
import productsList from './productsList';

export default history => combineReducers({
  router: connectRouter(history),
  products,
  productsList
});
