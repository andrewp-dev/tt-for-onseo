import { handleActions } from 'redux-actions';
import { addProduct, removeProduct } from '../actions';

const initialProductsState = [];

const products = handleActions({
    [addProduct]: (state, action) => {
      return [].concat(state, [{ ...action.payload }]);
    },
    [removeProduct]: (state, action) => {
      return [].concat(state, [{ ...action.payload }]);
    },
  },
  initialProductsState
);

export default products;
