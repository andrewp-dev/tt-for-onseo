export default function () {
    return [
      {
        id: 0,
        name: 'Product 1',
        photo: 'https://via.placeholder.com/200x150/?text=Product photo',
        price: 1,
      },
      {
        id: 1,
        name: 'Product 2',
        photo: 'https://via.placeholder.com/200x150/?text=Product photo',
        price: 2,
      },
      {
        id: 2,
        name: 'Product 3',
        photo: 'https://via.placeholder.com/200x150/?text=Product photo',
        price: 3,
      },
      {
        id: 3,
        name: 'Product 4',
        photo: 'https://via.placeholder.com/200x150/?text=Product photo',
        price: 4,
      },
      {
        id: 4,
        name: 'Product 5',
        photo: 'https://via.placeholder.com/200x150/?text=Product photo',
        price: 5,
      },
      {
        id: 5,
        name: 'Product 6',
        photo: 'https://via.placeholder.com/200x150/?text=Product photo',
        price: 6,
      }
    ]
  };
